package com.astronuun.spotholescommunity.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.astronuun.spotholescommunity.R
import com.astronuun.spotholescommunity.ui.introduction.IntroductionOneActivity
import java.util.Timer
import java.util.TimerTask

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handleLoading()
    }

    private fun handleLoading() {
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                startActivity(Intent(this@MainActivity, IntroductionOneActivity::class.java))
                finish()
            }
        }, 3000)
    }
}
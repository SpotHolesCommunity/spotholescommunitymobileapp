package com.astronuun.spotholescommunity.ui.introduction

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.astronuun.spotholescommunity.R

class IntroductionOneActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_introduction_one)
    }
}